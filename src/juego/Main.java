/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package juego;

/**
 * 
 * @author Brayan Guaman
 */
public class Main {
    public static void main(String[] args) {
        Jugador jugadorA = new Jugador("Jorge");
        Jugador jugadorB = new Jugador("Ismael");
        
        jugadorA.lanzarDado();
        jugadorB.lanzarDado();
        
        System.out.println("Jugador A: "+jugadorA.dado.numeroCara);
        System.out.println("Jugador B: "+jugadorB.dado.numeroCara);
        
        if( jugadorA.dado.numeroCara != jugadorB.dado.numeroCara ){
            System.out.print("EL GANADOR ES: ");
            System.out.println((jugadorA.dado.numeroCara > jugadorB.dado.numeroCara) ? jugadorA.nombre:
                    jugadorB.nombre);
        }
        
        
    }
}
