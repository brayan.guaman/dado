/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package juego;

/**
 * 
 * @author Brayan Guaman
 */
public class Jugador {
    
    String nombre;
    int apuesta;
    Dado dado = new Dado();
    

    public Jugador(String nombre){
        //apuesta por defecto de 10
        this(nombre,10);
    }
    
    public Jugador(String nombre, int apuesta) {
        this.nombre = nombre;
        this.apuesta = apuesta;
    }
    
    public void lanzarDado(){
        dado.obtieneCara();
    }

    public String getNombre() {
        return nombre;
    }

    public int getApuesta() {
        return apuesta;
    }

    public void setApuesta(int apuesta) {
        this.apuesta = apuesta;
    }
    
    

}
